Technologies and frameworks used:
*  JDK 8
*  Tomcat 8.5
*  Maven 4
*  Spring MVC 5.1.0
*  jstl 1.2
*  jackson-databind 2.9.4

Installation/Deployment:
1. Get the gitlab url.
2. Open Eclipse.
3. Click file.
4. Click import.
5. Search for git and select "Projects from Git" then click next.
6. Select Clone URI then click next.
7. Paste the gitlab url in URI field then click finish.
8. Wait project to finish building workspace.
9. Right click the project and run as on server.