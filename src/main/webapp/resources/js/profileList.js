function showProfileList() {
	fetch(ctx + '/profile')
	.then(function(response) {
		return response.json();
	}).then(function(data) {
		let profileTable = document.getElementById('profileTable');
		//Delete row that show loading data.
		profileTable.deleteRow(1);
		
		let dataLength = data.length;
		let i;
		for (i = 0; i < dataLength; i++) {
			let row = profileTable.insertRow(i+1);
			
			let nameCell = row.insertCell(0);
			let name = data[i].name.first + ' ' + data[i].name.last;
			nameCell.innerHTML = '<a href="javascript:;" onclick=showProfileDetails("' + data[i].id + '")>' + name + '</a>';
			
			let ageCell = row.insertCell(1);
			ageCell.innerHTML = data[i].age;
			
			let activeCell = row.insertCell(2);
			activeCell.innerHTML = '<input type="checkbox" ' + (data[i].active ? 'checked':'') +'></a>';
			
			let blockedCell = row.insertCell(3);
			blockedCell.innerHTML = '<input type="checkbox" ' + (data[i].blocked ? 'checked':'') +'></a>';
		}
	});
}

function showProfileDetails(id) {
	window.location.href = ctx + '/view/' + id;
}

function searchProfile() {
	
}


function sortTable(n) {
	var table, rows, switching, i, x, y, shouldSwitch, dir, switchcount = 0;
	table = document.getElementById("profileTable");
	switching = true;
	//Set the sorting direction to ascending:
	dir = "asc";
	/*Make a loop that will continue until
	no switching has been done:*/
	while (switching) {
		//start by saying: no switching is done:
		switching = false;
		rows = table.rows;
		/*Loop through all table rows (except the
		first, which contains table headers):*/
		for (i = 1; i < (rows.length - 1); i++) {
			//start by saying there should be no switching:
			shouldSwitch = false;
			/*Get the two elements you want to compare,
			one from current row and one from the next:*/
			x = rows[i].getElementsByTagName("TD")[n];
			y = rows[i + 1].getElementsByTagName("TD")[n];
			/*check if the two rows should switch place,
			based on the direction, asc or desc:*/
			if (dir == "asc") {
				//Handle if inside the cell is checkbox.
				if (x.firstElementChild.type == 'checkbox' 
					&& (x.firstElementChild.checked == false && y.firstElementChild.checked == true)) {
					//if so, mark as a switch and break the loop:
					shouldSwitch = true;
					break;
				} else if (x.innerHTML.toLowerCase() > y.innerHTML.toLowerCase()) {
					//if so, mark as a switch and break the loop:
					shouldSwitch = true;
					break;
				}
			} else if (dir == "desc") {
				//Handle if inside the cell is checkbox.
				if (x.firstElementChild.type == 'checkbox' 
					&& (x.firstElementChild.checked == true && y.firstElementChild.checked == false)) {
					//if so, mark as a switch and break the loop:
					shouldSwitch = true;
					break;
				} else if (x.innerHTML.toLowerCase() < y.innerHTML.toLowerCase()) {
					//if so, mark as a switch and break the loop:
					shouldSwitch = true;
					break;
				}
			}
		}
		if (shouldSwitch) {
			/*If a switch has been marked, make the switch
			and mark that a switch has been done:*/
			rows[i].parentNode.insertBefore(rows[i + 1], rows[i]);
			switching = true;
			//Each time a switch is done, increase this count by 1:
			switchcount++;
		} else {
			/*If no switching has been done AND the direction is "asc",
			set the direction to "desc" and run the while loop again.*/
			if (switchcount == 0 && dir == "asc") {
				dir = "desc";
				switching = true;
			}
		}
	}
}