<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix = "c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<html>
<head>
	<title>Profile</title>
	<link rel="stylesheet" href="resources/css/profileList.css">
	<script>var ctx = "/Profile"</script>
</head>
<body>
<br>
<br>

<input type="text" id="searchInput" class="search" onkeyup="searchProfile()" placeholder="search">
<table id="profileTable">
  <tbody><tr>
   	<th onclick="sortTable(0)">Name</th>
    <th onclick="sortTable(1)">Age</th>
    <th onclick="sortTable(2)">Active</th>
    <th onclick="sortTable(3)">Block</th>
  </tr>
  <tr>
  	<td style="text-align:center;" colspan="100%">Loading data...</td>
  </tr>
</tbody></table>

<script src="resources/js/profileList.js"></script>
<script>showProfileList();</script>

</body></html>