<html>
<head>
	<title>Profile</title>
	<link rel="stylesheet" type="text/css" href="/Profile/resources/css/viewProfile.css">
	<script>var ctx = "/Profile"</script>
</head>
<body>
<br>
<br>

<div class="profile-form">
	<div class="row">
		<img src="${profileDetails.picture}" alt="John" style="width:15%">
		<p class="title">${profileDetails.name.first} ${profileDetails.name.last}</p>
	</div>
	<br>
	<div class="row">
		<label>ID:</label>
		<p>${profileDetails.id}</p>
	</div>
	<div class="row">
		<label>Profile:</label>
		<p>${profileDetails.profile}</p>
	</div>
	<div class="row">
		<label>Email:</label>
		<p>${profileDetails.email}</p>
	</div>
	<div class="row">
		<label>Phone:</label>
		<p>${profileDetails.phone}</p>
	</div>
	<div class="row">
		<label>Address:</label>
		<p>${profileDetails.address}</p>
	</div>
	<div class="row">
		<label>Age:</label>
		<p>${profileDetails.age}</p>
	</div>
	<div class="row">
		<label>Balance:</label>
		<p>${profileDetails.balance}</p>
	</div>
</div>

</body></html>