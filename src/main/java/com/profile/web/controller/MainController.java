package com.profile.web.controller;

import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.servlet.ModelAndView;

import com.profile.web.entity.Profile;

@Controller
public class MainController {

    @GetMapping("/")
    public String index(Model model) {
        return "profileList";
    }

	@GetMapping("/profile")
	public ResponseEntity<?> getProfileList() {
		RestTemplate restTemplate = new RestTemplate();
		Profile[] profileList = restTemplate
				.getForObject("http://s3-ap-southeast-1.amazonaws.com/fundo/js/profiles.json", Profile[].class);
		return ResponseEntity.ok().body(profileList);
	}

	@GetMapping("/profile/{id}")
	public ResponseEntity<?> getProfile(@PathVariable("id") String id) {
		RestTemplate restTemplate = new RestTemplate();
		Profile[] profileList = restTemplate
				.getForObject("http://s3-ap-southeast-1.amazonaws.com/fundo/js/profiles.json", Profile[].class);

		for (int i = 0; i < profileList.length; i++) {
			if (profileList[i].getId().equals(id)) {
				return ResponseEntity.ok().body(profileList[i]);
			}
		}
		
		return null;
	}

	@GetMapping("/view/{id}")
	public ModelAndView getProfileList(@PathVariable("id") String id) {
		ModelAndView model = new ModelAndView("viewProfile");

		RestTemplate restTemplate = new RestTemplate();
		Profile[] profileList = restTemplate
				.getForObject("http://s3-ap-southeast-1.amazonaws.com/fundo/js/profiles.json", Profile[].class);

		for (int i = 0; i < profileList.length; i++) {
			if (profileList[i].getId().equals(id)) {
				model.addObject("profileDetails", profileList[i]);
				return model;
			}
		}

		return null;
	}

}